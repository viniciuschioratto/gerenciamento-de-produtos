$(function(){
	$('#newProduct input.btn-submit').click(function (event) {
		event.preventDefault();
		var form = $(this).parent().parent();
		var data = form.serialize();
		var url = form.attr('action');
		$.ajax({
			url: url,
			data: data,
			type: 'post',
			success: function(res){
				//location.reload();
				alert("Produto Inserido");
				form[0].reset();
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});

	$('#newCategory input.btn-submit').click(function (event) {
		event.preventDefault();
		var form = $(this).parent().parent();
		var data = form.serialize();
		var url = form.attr('action');
		$.ajax({
			url: url,
			data: data,
			type: 'post',
			success: function(res){
				//location.reload();
				alert("Categoria Inserida");
				form[0].reset();
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});

	$('#editProduct input.btn-submit').click(function (event) {
		event.preventDefault();
		var form = $(this).parent().parent();
		var data = form.serialize();
		var url = form.attr('action');
		$.ajax({
			url: url,
			data: data,
			type: 'post',
			success: function(res){
				alert("Produto atualizado");
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});

	$('#editCategory input.btn-submit').click(function (event) {
		event.preventDefault();
		var form = $(this).parent().parent();
		var data = form.serialize();
		var url = form.attr('action');
		console.log(data);
		console.log(url);
		
		$.ajax({
			url: url,
			data: data,
			type: 'post',
			success: function(res){
				alert("Categoria atualizada");
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});

	$('.product .action.delete a').click(function () {
		var id_produto = $(this).attr('data-id');
		var conteudo = $(".product[data-id="+id_produto+"]");
		$.ajax({
			url: 'deleteProduct',
			data: "id="+id_produto,
			type: 'post',
			success: function(res){
				$(conteudo).remove();
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});

	$('.categoria .action.delete a').click(function () {
		var id_categoria = $(this).attr('data-id');
		var conteudo = $(".categoria[data-id="+id_categoria+"]");
		$.ajax({
			url: 'deleteCategorie',
			data: "id="+id_categoria,
			type: 'post',
			success: function(res){
				$(conteudo).remove();
			},
			error: function(res){
				alert("Erro, tente novamente");
			}
		});
	});
});