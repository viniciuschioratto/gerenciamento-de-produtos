<?php

namespace Application\core;

use Application\models\Product;
use Application\models\Categorie;
use Application\models\Log;
use Application\models\ProductDAO;
use Application\models\CategorieDAO;
use Application\models\CatProdDAO;

class Controller
{

  public function model($model){
    require '../Application/models/' . $model . '.php';
    $classe = 'Application\\models\\' . $model;
    return new $classe();
  }

  public function view(string $view, $data = [])
  {
    require '../Application/views/' . $view . '.php';

  }

  public function pageNotFound()
  {
    $this->view('erro404');
  }
}
