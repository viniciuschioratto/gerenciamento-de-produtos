<?php

namespace Application\core;

use PDO;
class Database extends PDO
{
  private $conn = null; // Conexão com o banco de dados
  
  public static function getInstance(){
    if($conn == null){
      try {
        $conn   = new PDO('mysql:host=localhost;port=3306;dbname=gerenciamento_produto', 'root', 'admin');
        return $conn;
      } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
      }
    }else{
      return $conn;
    }
  }
}
