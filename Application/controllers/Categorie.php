<?php

use Application\core\Controller;
use Application\core\Database;

class Categorie extends Controller
{
	public function categories(){
		try{
			$Categorie = Controller::model('CategorieDAO');
			$conexao = Database::getInstance();
			$data = $Categorie::findAll($conexao);
			$this->view('categorie/categories', $data);
			$Log = Controller::model('Log');
  			$Log->logMsg('Lista Categoria',$data,'info','categorie', '../Application/log/ControllerCategorie.txt');
			$conexao = null;
		}catch(Exception $e){
			$Log->logMsg('Lista Categoria',$e->getMessage() ,'error','categorie', '../Application/log/ControllerCategorie.txt');
			//echo $e->getMessage();
			$conexao = null;
		}
	}

	public function addCategory(){
	    try{
	      if (isset($_POST) && count($_POST) > 0){
	        $categoria = Controller::model('Categorie');
	        $categoria->setCodigo($_POST['codigo']);
	        $categoria->setNome($_POST['name']);
	        $conexao = Database::getInstance();
	        $conexao->beginTransaction();
	        $new_categorie = Controller::model('CategorieDAO');
	        $new_categorie->Insert($categoria, $conexao);
	        $conexao->commit();
	        $conexao = null;
	        $Log = Controller::model('Log');
  			$Log->logMsg('Adiciona Categoria',$_POST,'info','addCategory', '../Application/log/ControllerCategorie.txt');
	        //echo json_encode(array('sucess' => 'Produto inserido com sucesso!!!'));
	      }else{
	        $this->view('categorie/addCategorie');
	      }
	    }catch (Eception $e){
	      $conexao->rollBack();
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Adiciona Categoria',$e->getMessage(),'error','addCategory', '../Application/log/ControllerCategorie.txt');
	      //echo json_encode(array('error' => $e->getMessage()));
	    }
  	}

  	public function deleteCategorie(){
	    try{
	      $Categorie = $this->model('CategorieDAO');
	      $conexao = Database::getInstance();
	      $conexao->beginTransaction();
	      $data = $Categorie::delete($conexao, $_POST['id']);
	      $conexao->commit();
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Deleta Categoria',$_POST,'info','deleteCategorie', '../Application/log/ControllerCategorie.txt');
	    }catch(Exception $e){
	      $conexao->rollBack();
	      //echo $e->getMessage();
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Deleta Categoria',$e->getMessage(),'error','deleteCategorie', '../Application/log/ControllerCategorie.txt');
	    }
  	}

  	public function editCategorie($id = null){
	    try{
	      $Categorie = Controller::model('CategorieDAO');
	      $conexao = Database::getInstance();
	      $data = $Categorie::findById($conexao, $id);
	      $this->view('categorie/editCategorie', $data);
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Edita Categoria',$data,'info','editCategorie', '../Application/log/ControllerCategorie.txt');
	    }catch(Exception $e){
	      //echo $e->getMessage();
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Edita Categoria',$e->getMessage(),'error','editCategorie', '../Application/log/ControllerCategorie.txt');
	    }
  	}

  	public function updateCategorie(){
	    try{
	      if (isset($_POST) && count($_POST) > 0){
	        $categoria = Controller::model('Categorie');
	        $categoria->setId($_POST['id']);
	        $categoria->setNome($_POST['name']);
	        $categoria->setCodigo($_POST['codigo']);
	        $conexao = Database::getInstance();
	        $conexao->beginTransaction();
	        $update_categorie = Controller::model('CategorieDAO');
	        $update_categorie->Update($categoria, $conexao);
	        $conexao->commit();
	        $conexao = null;
	        $Log = Controller::model('Log');
  		  	$Log->logMsg('Atualiza Categoria',$_POST,'info','updateCategorie', '../Application/log/ControllerCategorie.txt');
	        //echo json_encode(array('sucess' => 'Produto atualizado com sucesso!!!'));
	      }
	    }catch (Eception $e){
	      $conexao->rollBack();
	      $conexao = null;
	      $Log = Controller::model('Log');
  		  $Log->logMsg('Atualiza Categoria', $e->getMessage(),'error','updateCategorie', '../Application/log/ControllerCategorie.txt');
	      //echo json_encode(array('error' => $e->getMessage()));
	    }
  	}
}
