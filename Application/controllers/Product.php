<?php

use Application\core\Controller;
use Application\core\Database;

class Product extends Controller
{
  public function addProduct(){
    try{
      if (isset($_POST) && count($_POST) > 0){
        $product = Controller::model('Product');
        $product->setSku($_POST['sku']);
        $product->setNome($_POST['name']);
        $product->setPreco($_POST['price']);
        $product->setQuantidade($_POST['quantity']);
        $product->setDescricao($_POST['description']);
        $conexao = Database::getInstance();
        $conexao->beginTransaction();
        $new_product = Controller::model('ProductDAO');
        $id_produto = $new_product->Insert($product, $conexao);
        if (is_numeric($id_produto) && count($_POST['category']) > 0){
          $new_product_categorie = Controller::model('CatProdDAO');
          $new_product_categorie->Insert($id_produto, $_POST['category'], $conexao);
        }
        $conexao->commit();
        $conexao = null;
        $Log = Controller::model('Log');
        $Log->logMsg('Adiciona Produto',$_POST,'info','addProduct', '../Application/log/ControllerProduct.txt');
        //echo json_encode(array('sucess' => 'Produto inserido com sucesso!!!'));
      }else{
        $Categorias = $this->model('CategorieDAO');
        $conexao = Database::getInstance();
        $categ = $Categorias::findAll($conexao);
        $this->view('product/addProduct', $categ);
        $Log = Controller::model('Log');
        $Log->logMsg('Adiciona Produto',$categ,'info','addProduct', '../Application/log/ControllerProduct.txt');
      }
    }catch (Eception $e){
      $conexao->rollBack();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Adiciona Produto',$e->getMessage(),'error','addProduct', '../Application/log/ControllerProduct.txt');
      //echo json_encode(array('error' => $e->getMessage()));
    }
  }

  public function products(){
    try{
      $Product = Controller::model('ProductDAO');
      $conexao = Database::getInstance();
      $data = $Product::findAll($conexao);
      $this->view('product/products', $data);
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Lista Produtos',$data,'info','products', '../Application/log/ControllerProduct.txt');
    }catch(Exception $e){
      //echo $e->getMessage();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Lista Produtos',$e->getMessage(),'error','products', '../Application/log/ControllerProduct.txt');
    }
  }

  public function editProduct($id = null){
    try{
      $Product = Controller::model('ProductDAO');
      $conexao = Database::getInstance();
      $data = $Product::findById($conexao, $id);
      $Categorias = Controller::model('CategorieDAO');
      $categ = $Categorias::findAll($conexao);
      $data->categoria = $categ;
      $categoria_produto = Controller::model('CatProdDAO');
      $catProd = $categoria_produto->getCategoryByProduct($conexao, $id);
      $data->categoria_existente = $catProd;
      $this->view('product/editProduct', $data);
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Edita Produto',$data,'info','editProduct', '../Application/log/ControllerProduct.txt');
    }catch(Exception $e){
      //echo $e->getMessage();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Edita Produto',$e->getMessage(),'error','editProduct', '../Application/log/ControllerProduct.txt');
    }
  }

  public function deleteProduct(){
    try{
      $Product = $this->model('ProductDAO');
      $conexao = Database::getInstance();
      $conexao->beginTransaction();
      $data = $Product::delete($conexao, $_POST['id']);
      $conexao->commit();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Deleta Produto',$_POST,'info','deleteProduct', '../Application/log/ControllerProduct.txt');
    }catch(Exception $e){
      $conexao->rollBack();
      //echo $e->getMessage();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Deleta Produto',$e->getMessage(),'error','deleteProduct', '../Application/log/ControllerProduct.txt');
    }
  }

  public function updateProduct(){
    try{
      if (isset($_POST) && count($_POST) > 0){
        $product = Controller::model('Product');
        $product->setId($_POST['id']);
        $product->setSku($_POST['sku']);
        $product->setNome($_POST['name']);
        $product->setPreco($_POST['price']);
        $product->setQuantidade($_POST['quantity']);
        $product->setDescricao($_POST['description']);
        $conexao = Database::getInstance();
        $conexao->beginTransaction();
        $update_product = Controller::model('ProductDAO');
        $return_update = $update_product->Update($product, $conexao);
        if ($return_update){
          $update_product_categorie = Controller::model('CatProdDAO');
          $update_product_categorie->Update($_POST['id'], $_POST['category'], $conexao);
        }
        $conexao->commit();
        $conexao = null;
        $Log = Controller::model('Log');
        $Log->logMsg('Atualiza Produto',$_POST,'info','updateProduct', '../Application/log/ControllerProduct.txt');
        //echo json_encode(array('sucess' => 'Produto atualizado com sucesso!!!'));
      }
    }catch (Eception $e){
      $conexao->rollBack();
      $conexao = null;
      $Log = Controller::model('Log');
      $Log->logMsg('Atualiza Produto',$e->getMessage(),'error','updateProduct', '../Application/log/ControllerProduct.txt');
      //echo json_encode(array('error' => $e->getMessage()));
    }
  }
}
