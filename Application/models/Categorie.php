<?php

namespace Application\models;

class Categorie{
  private $id;
  private $nome;
  private $codigo;

  public function setId($id_categoria){
    $this->id = $id_categoria;
  }

  public function getId(){
    return $this->id;
  }

  public function setCodigo($codigo_categoria){
    $this->codigo = $codigo_categoria;
  }

  public function getCodigo(){
    return $this->codigo;
  }

  public function setNome($nome_categoria){
    $this->nome = $nome_categoria;
  }

  public function getNome(){
    return $this->nome;
  }
}
