<?php

namespace Application\models;

class Log{
  function logMsg( $msg, $data = NULL, $level = 'info', $class = NULL, $file = '../Application/log/main.txt' ){
    $levelStr = '';
    switch ( $level ){
          case 'info':
              $levelStr = 'INFO';
              break;
          case 'warning':
              $levelStr = 'WARNING';
              break;
          case 'error':
              $levelStr = 'ERROR';
              break;
      }
      $msg = $msg."\n".var_export($data, true);
      $date = date( 'Y-m-d H:i:s' );
      $msg = sprintf( "[%s] [%s] [%s]: %s%s", $date, $levelStr, $class, $msg, PHP_EOL );
      file_put_contents( $file, $msg, FILE_APPEND );
  }
}
