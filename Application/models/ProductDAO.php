<?php

namespace Application\models;

class ProductDAO{
  
  public static function findAll($conn)
  {
    try{
      $sql = "SELECT * FROM produto";
      $result = $conn->query($sql);
      while($raw = $result->fetchObject()){
        $sql = "SELECT 
                    `c`.`nome`
                FROM
                    categoria c
                INNER JOIN
                    categoria_produto ct ON ct.id_categoria = c.id
                        AND ct.id_produto = ?;";
        $stmt = $conn->prepare($sql);
        $stmt->bindValue(1,$raw->id);
        $stmt->execute();
        $raw->categoria = $stmt->fetchAll();
        $tags[] = $raw; 
      }
      return $tags;
    }catch(Exception $e){
      echo $e;
    }
  }

  public static function findById($conn, $id)
  {
    try{
      $sql = "SELECT * FROM `produto` WHERE `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id);
      $stmt->execute();
      return $stmt->fetchObject();
    }catch(PDOException $e){
      echo $e;
    }
  }

  public static function Insert($product, $conn){
    try{
      $sql = "INSERT INTO 
          `produto`
          (`nome`,      
           `sku`,
           `preco`,
           `descricao`,
           `quantidade`)
        VALUES
          (?,
           ?,
           ?,
           ?,
           ?)";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1 , $product->getNome());
      $stmt->bindValue(2 , $product->getSku());
      $stmt->bindValue(3 , $product->getPreco());
      $stmt->bindValue(4 , $product->getDescricao());
      $stmt->bindValue(5 , $product->getQuantidade());
      $stmt->execute();
      return $conn->lastInsertId();
    }catch(PDOException $e){
      echo $e;
      return false;
    }
  }


  public static function Update($product, $conn){
    try{
      $sql = "UPDATE `produto`
        SET
          `nome`= ?,      
           `sku` = ?,
           `preco` = ?,
           `descricao` = ?,
           `quantidade` = ?
        WHERE
            `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1 , $product->getNome());
      $stmt->bindValue(2 , $product->getSku());
      $stmt->bindValue(3 , $product->getPreco());
      $stmt->bindValue(4 , $product->getDescricao());
      $stmt->bindValue(5 , $product->getQuantidade());
      $stmt->bindValue(6 , $product->getId());
      $stmt->execute();
      return true;
    }catch(PDOException $e){
      echo $e;
      return false;
    }
  }

  public static function delete($conn, $id){
    try{
      $sql = "DELETE FROM `produto` WHERE `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id);
      $stmt->execute();
      return true;
    }catch(PDOException $e){
      echo $e;
    }
  }
}
