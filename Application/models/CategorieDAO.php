<?php

namespace Application\models;

class CategorieDAO{
  
  public static function findAll($conn)
  {
    try{
      $sql = "SELECT * FROM categoria";
      $result = $conn->query($sql);
      while($raw = $result->fetchObject()){
        $tags[] = $raw; 
      }
      return $tags;
    }catch(Exception $e){
      echo $e;
    }
  }

  public static function findById($conn, $id)
  {
    try{
      $sql = "SELECT * FROM `categoria` WHERE `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id);
      $stmt->execute();
      return $stmt->fetchObject();
    }catch(PDOException $e){
      echo $e;
    }
  }

  public static function Insert($categoria, $conn){
    try{
      $sql = "INSERT INTO `categoria` (`nome`, `codigo`) VALUES (?, ?)";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1 , $categoria->getNome());
      $stmt->bindValue(2 , $categoria->getCodigo());
      $stmt->execute();
      return true;
    }catch(PDOException $e){
      echo $e;
      return false;
    }
  }


  public static function Update($categoria, $conn){
    try{
      $sql = "UPDATE `categoria` SET `nome`= ?, `codigo` = ? WHERE `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1 , $categoria->getNome());
      $stmt->bindValue(2 , $categoria->getCodigo());
      $stmt->bindValue(3 , $categoria->getId());
      $stmt->execute();
      return true;
    }catch(PDOException $e){
      echo $e;
      return false;
    }
  }

  public static function delete($conn, $id){
    try{
      $sql = "DELETE FROM `categoria` WHERE `id` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id);
      $stmt->execute();
      return true;
    }catch(PDOException $e){
      echo $e;
    }
  }
}
