<?php

namespace Application\models;

class Product{

  private $id;
  private $sku;
  private $nome;
  private $preco;
  private $descricao;
  private $quantidade;

  public function setId($id_produto){
    $this->id = $id_produto;
  }

  public function getId(){
    return $this->id;
  }

  public function setSku($sku_produto){
    $this->sku = $sku_produto;
  }

  public function getSku(){
    return $this->sku;
  }

  public function setNome($nome_produto){
    $this->nome = $nome_produto;
  }

  public function getNome(){
    return $this->nome;
  }

  public function setPreco($preco_produto){
    $this->preco = $preco_produto;
  }

  public function getPreco(){
    return $this->preco;
  }

  public function setDescricao($descricao_produto){
    $this->descricao = $descricao_produto;
  }

  public function getDescricao(){
    return $this->descricao;
  }

  public function setQuantidade($quantidade_produto){
    $this->quantidade = $quantidade_produto;
  }

  public function getQuantidade(){
    return $this->quantidade;
  }
}
