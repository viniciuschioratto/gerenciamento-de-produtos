<?php

namespace Application\models;

class CatProdDAO{

  public static function Insert($product, $categories, $conn){
    try{
      $sql = 'INSERT INTO categoria_produto (id_produto,  id_categoria) VALUES ';
      $insertQuery = array();
      $insertData = array();
      foreach ($categories as $row) {
        $insertQuery[] = '(?, ?)';
        $insertData[] = $product;
        $insertData[] = $row;
      }
      if (!empty($insertQuery)) {
        $sql .= implode(', ', $insertQuery);
        $stmt = $conn->prepare($sql);
        $stmt->execute($insertData);
      }
    }catch(PDOException $e){
      echo $e;
    }
  }

  public static function getCategoryByProduct($conn, $id_prod){
    try{
      $sql = "SELECT * FROM `categoria_produto` WHERE `id_produto` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id_prod);
      $stmt->execute();
      while($raw = $stmt->fetchObject()){
        $tags[] = $raw->id_categoria;
      }
      return $tags;
    }catch(Exception $e){
      echo $e;
    }
  }

  public static function Update($id_prod, $categoria, $conn){
    try{
      $sql = "SELECT * FROM `categoria_produto` WHERE `id_produto` = ?";
      $stmt = $conn->prepare($sql);
      $stmt->bindValue(1,$id_prod);
      $stmt->execute();
      while($raw = $stmt->fetchObject()){
        if(!in_array($raw->id_categoria, $categoria)){
          $sql = "DELETE FROM `categoria_produto` WHERE `id` = ?";
          $stmt = $conn->prepare($sql);
          $stmt->bindValue(1 , $raw->id);
          $stmt->execute();
        }else{
          $posicao = array_search($raw->id_categoria, $categoria);
          if(is_numeric($posicao)){
            unset($categoria[$posicao]);
          }
        }
      }
      if(count($categoria) > 0){
        foreach ($categoria as $new_categoria) {
          $sql = "INSERT INTO `categoria_produto` (`id_produto`, `id_categoria`) VALUES (?, ?)";
          $stmt = $conn->prepare($sql);
          $stmt->bindValue(1 , $id_prod);
          $stmt->bindValue(2 , $new_categoria);
          $stmt->execute();
        }
      }
    }catch(Exception $e){
      echo $e;
    }
  }  

}
