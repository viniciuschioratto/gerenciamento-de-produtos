<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/"><img src="/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="../categories/categorie" class="link-menu">Categorias</a></li>
      <li><a href="products" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/" class="link-logo"><img src="/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>
<main class="content">
  <h1 class="title new-item">New Product</h1>
  <form id="newProduct" method="post" action="addProduct">
    <div class="input-field">
      <label for="sku" class="label">Product SKU</label>
      <input type="number" id="sku" class="input-text" name="sku" value=""/> 
    </div>
    <div class="input-field">
      <label for="name" class="label">Product Name</label>
      <input type="text" id="name" class="input-text" name="name" value=""/> 
    </div>
    <div class="input-field">
      <label for="price" class="label">Price</label>
      <input type="number" step="0.01" id="price" class="input-text" name="price" value=""/> 
    </div>
    <div class="input-field">
      <label for="quantity" class="label">Quantity</label>
      <input type="number" id="quantity" class="input-text" name="quantity" value=""/> 
    </div>
    <div class="input-field">
      <label for="category" class="label">Categories</label>
      <select multiple id="category" class="input-text" name="category[]">
      <?php
        if(count($data) > 0){
          foreach ($data as $dados) {
      ?>
        <option value="<?php echo $dados->id; ?>"><?php echo $dados->nome; ?></option>
      <?php
        }
          } 
      ?>
      </select>
    </div>
    <div class="input-field">
      <label for="description" class="label">Description</label>
      <textarea id="description" class="input-text" name="description" value=""></textarea>
    </div>
    <div class="actions-form">
      <a href="/product/products" class="action back">Back</a>
      <input class="btn-submit btn-action" type="submit" value="Save Product" />
    </div>
  </form>
</main>

