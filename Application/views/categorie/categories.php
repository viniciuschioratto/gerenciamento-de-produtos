<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="/"><img src="/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <li><a href="categories" class="link-menu">Categorias</a></li>
      <li><a href="../product/products" class="link-menu">Produtos</a></li>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="/" class="link-logo"><img src="/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<main class="content">
<div class="header-list-page">
  <h1 class="title">Categories</h1>
  <a href="addCategory" class="btn-action">Add new Category</a>
</div>
<table class="data-grid">
  <tr class="data-row">
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Name</span>
    </th>
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Code</span>
    </th>
    <th class="data-grid-th">
        <span class="data-grid-cell-content">Actions</span>
    </th>
  </tr>
  <?php
    if(count($data) > 0){
      foreach ($data as $dados) {
  ?>
	<tr class="data-row categoria" data-id="<?php echo $dados->id; ?>">
		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $dados->nome; ?></span>
		</td>
		<td class="data-grid-td">
			<span class="data-grid-cell-content"><?php echo $dados->codigo; ?></span>
		</td>
		<td class="data-grid-td">
			<div class="actions">
				<div class="action edit"><a href="editCategorie/<?php echo $dados->id; ?>"><span>Edit</span></a></div>
            	<div class="action delete"><a data-id="<?php echo $dados->id; ?>"><span>Delete</span></a></div>
			</div>
		</td>
	</tr>
	<?php
	   }
	    } 
	?>
</table>
</main>
