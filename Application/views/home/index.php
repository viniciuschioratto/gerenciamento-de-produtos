<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
	<div class="close-menu">
		<a on="tap:sidebar.toggle">
			<img src="/assets/images/bt-close.png" alt="Close Menu" width="24" height="24" />
		</a>
	</div>
	<a href=""><img src="/assets/images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
	<div>
		<ul>
			<li><a href="categorie/categories" class="link-menu">Categorias</a></li>
			<li><a href="product/products" class="link-menu">Produtos</a></li>
		</ul>
	</div>
</amp-sidebar>
<header>
	<div class="go-menu">
		<a on="tap:sidebar.toggle">☰</a>
		<a href="/" class="link-logo"><img src="/assets/images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
	</div>
	<div class="right-box">
		<span class="go-title">Administration Panel</span>
	</div>    
</header>
<main class="content">
	<div class="header-list-page">
		<h1 class="title">Dashboard</h1>
	</div>
	<div class="infor">
		You have 4 products added on this store: <a href="product/addProduct" class="btn-action">Add new Product</a>
	</div>
	<ul class="product-list">
		<li>
			<div class="product-image">
				<img src="/assets/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
			</div>
			<div class="product-info">
				<div class="product-name"><span>Tênis Runner Bolt</span></div>
				<div class="product-price"><span class="special-price">9 available</span> <span>R$459,99</span></div>
			</div>
		</li>
		<li>
			<div class="product-image">
				<img src="/assets/images/product/tenis-basket-light.png" layout="responsive" width="164" height="145" alt="Tênis Basket Light" />
			</div>
			<div class="product-info">
				<div class="product-name"><span>Tênis Basket Light</span></div>
				<div class="product-price"><span class="special-price">1 available</span> <span>R$459,99</span></div>
			</div>
		</li>
		<li>
			<div class="product-image">
				<img src="/assets/images/product/tenis-2d-shoes.png" layout="responsive" width="164" height="145" alt="Tênis 2D Shoes" />
			</div>
			<div class="product-info">
				<div class="product-name"><span>Tênis 2D Shoes</span></div>
				<div class="product-price"><span class="special-price">2 Available</span> <span>R$459,99</span></div>
			</div>
		</li>
		<li>
			<div class="product-image">
				<img src="/assets/images/product/tenis-sneakers-43n.png" layout="responsive" width="164" height="145" alt="Tênis Sneakers 43N" />
			</div>
			<div class="product-info">
				<div class="product-name"><span>Tênis Sneakers 43N</span></div>
				<div class="product-price"><span class="special-price">Out of stock</span> <span>R$459,99</span></div>
			</div>
		</li>
	</ul>
</main>