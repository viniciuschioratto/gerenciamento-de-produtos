CREATE DATABASE gerenciamento_produto DEFAULT CHARACTER SET utf8;

CREATE TABLE `gerenciamento_produto`.`categoria`(
id INT(10) PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(255) DEFAULT NULL,
codigo INT(10) UNIQUE NOT NULL
) ENGINE = INNODB;

CREATE TABLE `gerenciamento_produto`.`produto`(
id INT(10) PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(255) DEFAULT NULL,
sku INT(20) DEFAULT NULL,
preco DECIMAL(10,2) DEFAULT 0.00,
descricao TEXT DEFAULT NULL,
quantidade INT(10) DEFAULT 0
) ENGINE = INNODB;

CREATE TABLE `gerenciamento_produto`.`categoria_produto`(
id INT(10) PRIMARY KEY AUTO_INCREMENT,
id_produto INT(10) NOT NULL,
id_categoria INT(10) NOT NULL,
CONSTRAINT fk_categoria FOREIGN KEY (id_categoria) REFERENCES `gerenciamento_produto`.`categoria` (id) ON DELETE CASCADE,
CONSTRAINT fk_produto FOREIGN KEY (id_produto) REFERENCES `gerenciamento_produto`.`produto` (id) ON DELETE CASCADE
) ENGINE = INNODB;